# numerical

Mata Kuliah
EW4004 - Metode Numerik (Numerical Methods)
Semester Ganjil - 2018 - 2019
Pengajar: Atar F. Babgei

Pada repository ini anda dapat menemukan bahan ajar dan tugas mata kuliah mengenai "Numerical Methods" untuk bagian sebelum pertengahan semester.
Silahkan cek repository ini secara periodik.

(01/10/2018)
Dear All,

Tugas kedua sudah diunggah, Deadline: 08-10-2018

Selamat Mengerjakan,
afb

(29/09/2018)
Dear All,

Untuk Tugas kedua (Assignment 2) akan saya unggah pada hari Senin 01/10/2018.
Batas akhir pengumpulannya juga akan diundur menjadi 1 minggu setelahnya 08/10/2018.
Seperti yang saya sampaikan pada pertemuan sebelumnya, minggu depan kita akan mulai berlatih pemrograman python untuk menyelesaikan problem matematik dengan metode numerik.
Silahkan coba memfamiliarkan diri terlebih dahulu dengan python untuk memudahkan anda mengikuti perkuliahan selanjutnya.

Selamat berakhir pekan,
afb

(15/09/2018)

Dear All,

Saya sudah upload tugas pertama untuk Mata Kuliah Metode Numerik.

Adapun Tata Cara Pengumpulan Tugas
- Cetak "Form Pengumpulan Tugas" dan isi bagian I
- Lampirkan form tersebut bersama tugas yang anda kerjakan (di staples dan diletakkan di bagian paling depan)
- Kumpulkan sesuai dengan deadline yang ditentukan
Apabila ada keterlambatan akan dikenai pinalti nilai, dengan rumus sbb:
Nilai Akhir = Nilai Tugas * (1 - 0.2 * n)

n = Jumlah hari terlambat; n <= 5 

Apabila ada yang kurang jelas dari tugas tersebut, silahkan ditanyaka langsung via email ke [**atarbabgei@gmail.com**](mailto:atarbabgei@gmail.com)<br>

Selamat Mengerjakan!