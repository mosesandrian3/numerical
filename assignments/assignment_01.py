import sympy as sy
import numpy as np
from sympy.functions import sin, cos, ln
import matplotlib.pyplot as plt
plt.style.use("ggplot")

def plot(f, x0 = 0, x_lims = [-4, 4], y_lims = [-4, 4], npoints = 400, x = sy.Symbol('x')):
    x1 = np.linspace(x_lims[0], x_lims[1], npoints)
    func_lambda = sy.lambdify(x, f, "numpy")
    plt.plot(x1, func_lambda(x1), label = 'Sin (x)')
    plt.xlim(x_lims)
    plt.ylim(y_lims)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend()
    plt.grid(True)
    #put your own student number on the following line
    plt.title('NRP: 7311740000001')
    plt.show()

x = sy.Symbol('x')
f = sin(x)

#plot the function of f
plot(f)